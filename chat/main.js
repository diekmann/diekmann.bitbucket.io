"use strict";

// based on https://github.com/mdn/samples-server/blob/master/s/webrtc-simple-datachannel/main.js


const startButton = document.getElementById('startButton');
const disconnectButton = document.getElementById('disconnectButton');

const logArea = document.getElementById("logarea");
function logTxt(txt) {
    logArea.value += "\n" + txt;
    logArea.scrollTop = logArea.scrollHeight;
};

const createOfferButton = document.getElementById('createOfferButton');

const connectRemoteOfferInputBox = document.getElementById('remoteOffer');
const createAnswerButton = document.getElementById('createAnswerButton');

const acceptAnswerInputBox = document.getElementById('remoteAnswer');
const acceptAnswerButton = document.getElementById('acceptAnswerButton');


const sendButton = document.getElementById('sendButton');
const sendMessageForm = document.getElementById('sendMessageForm');

const messageInputBox = document.getElementById('inputmessage');
const receiveBox = document.getElementById('receivebox');

function appendChatBox(txt, cssClass) {
    const el = document.createElement("p");
    el.className = cssClass;
    const txtNode = document.createTextNode(txt);
    el.appendChild(txtNode);
    receiveBox.appendChild(el);
}

// 'peer1' or 'peer2'
let whoAmI;
function otherPeer() {
    if (whoAmI == 'peer1') {
        return 'peer2';
    } else if (whoAmI == 'peer2') {
        return 'peer1';
    } else {
        return 'unknown_peer';
    }
};

let localConnection = null; // RTCPeerConnection
let sendChannel = null; // RTCDataChannel for the chat



let theOffer = {
    candidates: [],
    offer: "",
};
const theOfferRedraw = function() {
    const str = JSON.stringify(theOffer);
    document.getElementById("offer").innerText = str;
};
theOfferRedraw();


let theAnswer = {
    candidates: [],
    answer: "",
};
const theAnswerRedraw = function() {
    const str = JSON.stringify(theAnswer);
    document.getElementById("answer").innerText = str;
};
theAnswerRedraw();


// Set event listeners for user interface widgets.


// Create the local connection and its event listeners
startButton.addEventListener('click', function() {
    // Without a stun server, we will only get .local candidates.
    localConnection = new RTCPeerConnection({
        'iceServers': [{
            'urls': 'stun:stun.l.google.com:19302'
        }]
    });
    logTxt(`Connection state is ${localConnection.connectionState}`);

    localConnection.onsignalingstatechange = function(event) {
        logTxt("Signaling state change: " + localConnection.signalingState);
    };

    localConnection.oniceconnectionstatechange = function(event) {
        logTxt("ICE connection state change: " + localConnection.iceConnectionState);
    };

    // Create the data channel and establish its event listeners
    sendChannel = localConnection.createDataChannel("sendChannel");
    sendChannel.onopen = sendChannel.onclose = function (event) {
        // handleSendChannelStatusChange
        if (sendChannel) {
            const state = sendChannel.readyState;
            logTxt("Send channel's status has changed to " + state);

            if (state === "open") {
                messageInputBox.disabled = false;
                messageInputBox.focus();
                sendButton.disabled = false;
                disconnectButton.disabled = false;
                startButton.disabled = true;
            } else {
                messageInputBox.disabled = true;
                sendButton.disabled = true;
                startButton.disabled = false;
                disconnectButton.disabled = true;
            }
        }
    };

    localConnection.ondatachannel = function(event) {
        const c = event.channel;
        logTxt(`The channel should be open now: ${c.readyState}`);
        logTxt(`Connection state should be connected: ${localConnection.connectionState}`);
        // Receiving a message.
        c.onmessage = function (event) {
                const remotePeerName = otherPeer();
                console.log(`handling received message from ${remotePeerName}.`)
                appendChatBox(`From ${remotePeerName}: ${event.data}`, remotePeerName);
            };
    };

    // Set up the ICE candidates for the two peers
    localConnection.onicecandidate = function(event) {
        const c = event.candidate;
        if (c) {
            // Empty candidate signals end of candidates.
            if (c.candidate) {
                // You will need to send this candidate to the remote peer.
                logTxt(`ICE candidate ${c.protocol} ${c.ip}:${c.port}`);
            }

            if (whoAmI == 'peer1') {
                theOffer.candidates.push(c);
                theOfferRedraw();
            } else if (whoAmI == 'peer2') {
                theAnswer.candidates.push(c);
                theAnswerRedraw();
            } else {
                logTxt("ERROR: do you want to be peer1 or peer2?")
            }
        } else {
            console.log("All ICE candidates have been sent");
        }
    };
    
    createOfferButton.disabled = false;
    createAnswerButton.disabled = false;
}, false);


// Create offer.
createOfferButton.addEventListener('click', function() {
    whoAmI = 'peer1';
    // If we are peer1, all peer2 actions should be disabled.
    createAnswerButton.disabled = true;

    const offer = localConnection.createOffer()
        .then(offer => {
            console.log("offer created");
            theOffer.offer = offer;
            theOfferRedraw();
            return localConnection.setLocalDescription(offer)
        })
        .catch(handleError);

    // And now we wait for an answer.
    createOfferButton.disabled = true;
    acceptAnswerButton.disabled = false;
}, false);


// Create Answer.
createAnswerButton.addEventListener('click', function () {
    whoAmI = 'peer2';
    // If we are peer2, all peer1 actions should be disabled.
    createOfferButton.disabled = true;

    const v = JSON.parse(connectRemoteOfferInputBox.value);
    const candidates = v.candidates;
    const offer = v.offer;
    console.log(`createAnswer candidates: "${JSON.stringify(candidates)}" offer:"${JSON.stringify(offer)}"`);

    localConnection.setRemoteDescription(offer).catch(handleError);

    for (let i = 0; i < candidates.length; ++i) {
        localConnection.addIceCandidate(candidates[i]).then(console.log("candidate from remote added."))
            .catch(handleError);
    };


    localConnection.createAnswer()
        .then(answer => {
            console.log("answer created");
            theAnswer.answer = answer;
            theAnswerRedraw();
            return localConnection.setLocalDescription(answer)
        })
        .catch(handleError);

    createAnswerButton.disabled = true;
}, false);


// Accept Answer
acceptAnswerButton.addEventListener('click', function() {
    createOfferButton.disabled = true;

    const a = JSON.parse(acceptAnswerInputBox.value);
    const answer = a.answer;
    const candidates = a.candidates;

    console.log(`acceptAnswer candidate: "${JSON.stringify(candidates)}"`);

    localConnection.setRemoteDescription(answer).catch(handleError);

    for (let i = 0; i < candidates.length; ++i) {
        localConnection.addIceCandidate(candidates[i]).catch(handleError);
    };
}, false);

// Handles clicks on the "Send" button by transmitting a message.
sendMessageForm.addEventListener('submit', function(event) {
    console.log(`sendng message to ${otherPeer()}.`)

    // don't actually submit the HTML form, stay on the same page.
    event.preventDefault();

    const message = messageInputBox.value;
    sendChannel.send(message);

    appendChatBox(`From ${whoAmI}: ${message}`, whoAmI);

    // Clear the input box and re-focus it, so that we're
    // ready for the next message.
    messageInputBox.value = "";
    messageInputBox.focus();
}, false);


// Disconnect Peers
disconnectButton.addEventListener('click', function() {
    // Close the connection, including data channels if they're open.
    // Also update the UI to reflect the disconnected status.
    alert("TODO");
}, false);




function handleError(error) {
    const s = "ERROR: " + error.toString()
    console.log(s);
    logTxt(s);
}


